export function isTokenValid(token: string) {
  if (!token) {
    return false;
  }
  const tokenParts = token.split('.');
  if (tokenParts.length !== 3) {
    return false;
  }

  try {
    const payload = JSON.parse(atob(tokenParts[1]));
    const exp = payload.exp;
    const currentTime = Math.floor(Date.now() / 1000);

    return exp > currentTime;
  } catch (e) {
    return false;
  }
}
