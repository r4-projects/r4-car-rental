import { defineStore } from 'pinia';
import cars from '../assets/cars.json';
import { useTravelSettingsStore } from './travel-settings-store';

const TSS = useTravelSettingsStore();

interface Car {
  name: string;
  value_new: number;
  price_per_hour: number;
  price_per_day: number;
  category: string;
  number_of_seats: number;
  number_of_doors: number;
  gearbox_type: string;
  vehicle_type: string;
  fuel_type: string | null;
  autonomy: string;
  permit_obtained_since: number;
  other_properties: string[];
  photo_link: string;
}

interface Filters {
  category: string | null;
  number_of_seats: number | null;
  gearbox_type: string | null;
  vehicle_type: string | null;
  priceSort: string | null;
}

interface Option {
  label: string;
  value: string | number | string[] | null;
}

interface carStoreState {
  baseCars: Car[];
  filters: Filters;
  filteredCars: Car[];
  selectedCar: Car | null;
  selectedCategory: string | null;
  selectedSeats: number | null;
  selectedGearbox: string | null;
  selectedVehicleType: string | null;
  priceSort: string | null;
  categoriesOptions: Option[];
  seatsOptions: Option[];
  gearboxOptions: Option[];
  vehicleTypeOptions: Option[];
}

export const useCarStore = defineStore('carStore', {
  state: (): carStoreState => ({
    filters: {
      category: null,
      number_of_seats: null,
      gearbox_type: null,
      vehicle_type: null,
      priceSort: null,
    },
    selectedCar: null,
    filteredCars: [],
    selectedCategory: null,
    selectedSeats: null,
    selectedGearbox: null,
    selectedVehicleType: null,
    priceSort: null,
    categoriesOptions: [],
    seatsOptions: [],
    gearboxOptions: [],
    vehicleTypeOptions: [],
    baseCars: cars,
  }),
  getters: {
    uniqueCategories(): Option[] {
      return [...new Set(this.filteredCars.map((car) => car.category))].map(
        (category) => ({
          label: String(category),
          value: category,
        })
      );
    },
    uniqueSeats(): Option[] {
      return [...new Set(this.filteredCars.map((car) => car.number_of_seats))]
        .sort((a, b) => a - b)
        .map((seats) => ({
          label: String(`${seats} places`),
          value: seats,
        }));
    },
    uniqueGearboxTypes(): Option[] {
      return [...new Set(this.filteredCars.map((car) => car.gearbox_type))].map(
        (gearboxType) => ({
          label: String(gearboxType),
          value: gearboxType,
        })
      );
    },
    uniqueVehicleTypes(): Option[] {
      return [...new Set(this.filteredCars.map((car) => car.vehicle_type))].map(
        (vehicleType) => ({ label: String(vehicleType), value: vehicleType })
      );
    },
  },
  actions: {
    updateOptions(): void {
      const isAnyFilterSelected =
        this.selectedCategory !== null ||
        this.selectedGearbox !== null ||
        this.selectedSeats !== null ||
        this.selectedVehicleType !== null;
      if (isAnyFilterSelected) {
        const filteredCars = this.baseCars.filter(
          (car) =>
            (this.selectedCategory === null ||
              car.category === this.selectedCategory) &&
            (this.selectedGearbox === null ||
              car.gearbox_type === this.selectedGearbox) &&
            (this.selectedSeats === null ||
              car.number_of_seats === this.selectedSeats) &&
            (this.selectedVehicleType === null ||
              car.vehicle_type === this.selectedVehicleType)
        );
        const getUniqueOptions = <T extends keyof Car>(field: T): Option[] => {
          if (field === 'number_of_seats') {
            return Array.from(
              new Set(filteredCars.map((car) => car.number_of_seats))
            )
              .sort((a, b) => a - b)
              .map((value) => ({ label: String(value), value }));
          } else {
            return Array.from(
              new Set(filteredCars.map((car) => car[field]))
            ).map((value) => ({ label: String(value), value }));
          }
        };
        this.categoriesOptions = getUniqueOptions('category');
        this.seatsOptions = getUniqueOptions('number_of_seats');
        this.gearboxOptions = getUniqueOptions('gearbox_type');
        this.vehicleTypeOptions = getUniqueOptions('vehicle_type');
      } else {
        this.categoriesOptions = this.uniqueCategories;
        this.seatsOptions = this.uniqueSeats;
        this.gearboxOptions = this.uniqueGearboxTypes;
        this.vehicleTypeOptions = this.uniqueVehicleTypes;
      }
    },
    applyFilters(): void {
      this.filters = {
        category: this.selectedCategory,
        number_of_seats: this.selectedSeats,
        gearbox_type: this.selectedGearbox,
        vehicle_type: this.selectedVehicleType,
        priceSort: this.priceSort,
      };
      this.filterCars();
    },
    resetFilters(): void {
      this.filters = {
        category: null,
        number_of_seats: null,
        gearbox_type: null,
        vehicle_type: null,
        priceSort: null,
      };
      this.selectedCategory = null;
      this.selectedSeats = null;
      this.selectedGearbox = null;
      this.selectedVehicleType = null;
      this.priceSort = null;
      this.filteredCars = this.baseCars;
      // this.filterCars();
      this.updateOptions();
    },
    filterCars(): Car[] {
      this.filteredCars = this.baseCars.filter((car: Car) => {
        return (
          (this.filters.category === null ||
            car.category === this.filters.category) &&
          (this.filters.number_of_seats === null ||
            car.number_of_seats === this.filters.number_of_seats) &&
          (this.filters.gearbox_type === null ||
            car.gearbox_type === this.filters.gearbox_type) &&
          (this.filters.vehicle_type === null ||
            car.vehicle_type === this.filters.vehicle_type)
        );
      });
      if (this.filters.priceSort !== null) {
        this.filteredCars = this.filteredCars.sort((a, b) => {
          if (this.filters.priceSort === 'asc') {
            return a.price_per_day - b.price_per_day;
          } else if (this.filters.priceSort === 'desc') {
            return b.price_per_day - a.price_per_day;
          }
          return 0;
        });
      }
      return this.filteredCars;
    },
    returnCarsIfUnder25(): Car[] {
      this.baseCars = cars.filter((car: Car) => {
        return car.category === 'Citadine';
      });
      return this.filterCars();
    },
    returnCarsAccordingToPermitAge(): Car[] {
      this.baseCars = cars.filter((car: Car) => {
        if (TSS.yearsOfPermit) {
          return car.permit_obtained_since <= TSS.yearsOfPermit * 12;
        } else {
          return car.permit_obtained_since <= 12;
        }
      });
      return this.filterCars();
    },
    initializeCars() {
      if (TSS.moreThan25 === false) {
        this.returnCarsIfUnder25();
      } else if (TSS.moreThan25 === true && TSS.yearsOfPermit !== null) {
        this.returnCarsAccordingToPermitAge();
      }
      this.updateOptions();
    },
  },
});
