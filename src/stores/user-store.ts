import { defineStore } from 'pinia';

interface Token {
  token: string | null;
}
export const useUserStore = defineStore('user', {
  state: (): Token => ({
    token: null,
  }),
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    setToken(newToken: string | null) {
      this.token = newToken;
    },

    clearToken() {
      this.token = null;
    },
  },
});
