import { defineStore } from 'pinia';
import options from '../assets/options.json';

interface Option {
  title: string;
  price: number;
  selected: boolean;
  type: string;
  description: string;
}

interface OptionStoreState {
  options: Option[];
}

export const useOptionsStore = defineStore('optionsStore', {
  state: (): OptionStoreState => ({
    options: options,
  }),
  getters: {},
  actions: {
    updateKilometersOptions(option: Option) {
      let i = 0;
      if (option.selected === true) {
        option.selected = false;
      } else {
        for (i; i <= 3; i++) {
          if (options[i].selected === true) {
            options[i].selected = false;
          }
        }
        option.selected = !option.selected;
      }
    },
  },
});
