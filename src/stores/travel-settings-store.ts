import { defineStore } from 'pinia';

interface Locale {
  days: string[];
  daysShort: string[];
  months: string[];
  monthsShort: string[];
  firstDayOfWeek: number;
  format24h: boolean;
  pluralDay: string;
}

interface city {
  nom: string;
  code: string;
  codeDepartement: string;
  siren: string;
  codeEpci: string;
  codeRegion: string;
  codesPostaux: string[];
  population: number;
}

interface State {
  searchInput: string | null;
  activeSearch: boolean;
  cityCode: string | null;
  city: city | null;
  cities: object[] | null;
  cityList: boolean;
  departureDate: string | null;
  departureTime: string | null;
  returnDate: string | null;
  returnTime: string | null;
  departureTimeMenu: boolean;
  returnTimeMenu: boolean;
  departureDateMenu: boolean;
  returnDateMenu: boolean;
  myLocale: Locale;
  moreThan25: boolean;
  yearsOfPermit: number | null;
  searchTimeout: ReturnType<typeof setTimeout> | null;
  abortController: AbortController | null;
  rentalDays: number | null;
}

export const useTravelSettingsStore = defineStore('travelSettings', {
  state: (): State => ({
    searchInput: null,
    activeSearch: false,
    cityCode: null,
    cities: null,
    city: null,
    cityList: false,
    departureDate: null,
    departureTime: null,
    returnDate: null,
    returnTime: null,
    departureTimeMenu: false,
    returnTimeMenu: false,
    departureDateMenu: false,
    returnDateMenu: false,
    myLocale: {
      /* Start with Sunday */
      days: 'Dimanche_Lundi_Mardi_Mercredi_Jeudi_Vendredi_Samedi'.split('_'),
      daysShort: 'Dim_Lun_Mar_Mer_Jeu_Ven_Sam'.split('_'),
      months:
        'Janvier_Février_Mars_Avril_Mai_Juin_Juillet_Août_Septembre_Octobre_Novembre_Décembre'.split(
          '_'
        ),
      monthsShort: 'Jan_Fév_Mar_Avr_Mai_Juin_Juil_Août_Sept_Oct_Nov_Déc'.split(
        '_'
      ),
      firstDayOfWeek: 1, // 0-6, 0 - Sunday, 1 Monday, ...
      format24h: true,
      pluralDay: 'jours',
    },
    moreThan25: false,
    yearsOfPermit: null,
    searchTimeout: null,
    abortController: null,
    rentalDays: null,
  }),

  getters: {
    todaysday: (): string => new Date().getDate().toString().padStart(2, '0'),
    todaysMonth: (): string =>
      (new Date().getMonth() + 1).toString().padStart(2, '0'),
    todaysYear: (): string => new Date().getFullYear().toString(),
    actualHour: (): number => new Date().getHours(),
    actualMinute: (): number => new Date().getMinutes(),

    today: (): string => {
      const todaysYear = new Date().getFullYear().toString();
      const todaysMonth = (new Date().getMonth() + 1)
        .toString()
        .padStart(2, '0');
      const todaysday = new Date().getDate().toString().padStart(2, '0');
      return `${todaysYear}/${todaysMonth}/${todaysday}`;
    },
    todayFormatted: (): string => {
      const todaysYear = new Date().getFullYear().toString();
      const todaysMonth = (new Date().getMonth() + 1)
        .toString()
        .padStart(2, '0');
      const todaysday = new Date().getDate().toString().padStart(2, '0');
      return `${todaysday}/${todaysMonth}/${todaysYear}`;
    },
  },
  actions: {
    getNextDay(dateString: string): string {
      // Convertir la chaîne de caractères en un objet Date
      const [year, month, day] = dateString
        .split('/')
        .map((part) => parseInt(part, 10));
      const date = new Date(year, month - 1, day); // Les mois commencent à 0 en JavaScript
      // Ajouter un jour
      date.setDate(date.getDate() + 1);

      // Formater la nouvelle date au format YYYY/MM/DD
      const tomorrowYear = date.getFullYear().toString();
      const tomorrowMonth = (date.getMonth() + 1).toString().padStart(2, '0');
      const tomorrowDay = date.getDate().toString().padStart(2, '0');
      return `${tomorrowYear}/${tomorrowMonth}/${tomorrowDay}`;
    },
    defaultYearAndMonthReturn() {
      const defaultMonth = this.departureDate?.split('/')[1];
      const defaultYear = this.departureDate?.split('/')[2];
      return `${defaultYear}/${defaultMonth}`;
    },

    dateOptionsDeparture(date: string) {
      // has to compare with a format of YYYY/MM/DD
      if (this.actualHour === 23 && this.actualMinute >= 35) {
        return date >= this.getNextDay(this.today);
      } else {
        return date >= this.today;
      }
    },
    formatDepartureDate() {
      if (this.departureDate) {
        const [DD, MM, YYYY] = this.departureDate.split('/');
        return `${YYYY}/${MM}/${DD}`;
      }
    },
    dateOptionsReturn(date: string) {
      // has to compare with a format of YYYY/MM/DD
      const formattedDepartureDate = this.formatDepartureDate() as string;
      if (this.departureDate && this.departureTime) {
        const hourDeparture = parseInt(this.departureTime.split(':')[0], 10);
        if (hourDeparture === 23) {
          return date >= this.getNextDay(formattedDepartureDate);
        }
        if (this.departureDate && typeof this.departureDate === 'string') {
          return date >= formattedDepartureDate;
        }
      }
    },

    timeOptionsDeparture(hr: number, min: number) {
      const allowedMinutes = [0, 15, 30, 45];
      if (hr < this.actualHour && this.todayFormatted === this.departureDate) {
        return false;
      }
      if (
        hr <= this.actualHour &&
        this.actualMinute >= 35 &&
        this.todayFormatted === this.departureDate
      ) {
        return false;
      }
      if (min !== null && !allowedMinutes.includes(min)) {
        return false;
      }
      if (
        min !== null &&
        hr === this.actualHour &&
        min < this.actualMinute + 10 &&
        this.todayFormatted === this.departureDate
      ) {
        return false;
      }
      return true;
    },

    timeOptionsReturn(hr: number, min: number) {
      const allowedMinutes = [0, 15, 30, 45];
      const nextDay = this.getNextDay(this.departureDate as string);
      const [YYYY, MM, DD] = nextDay
        .split('/')
        .map((part) => parseInt(part, 10));
      const formattedNextDay = `${DD}/${MM}/${YYYY}`;
      if (this.departureTime) {
        const [HH, mm] = this.departureTime
          .split(':')
          .map((part: string) => parseInt(part, 10));
        if (hr < HH + 1 && this.departureDate === this.returnDate) {
          return false;
        }
        if (
          HH === 23 &&
          hr === 0 &&
          min < mm &&
          this.returnDate === formattedNextDay
        ) {
          return false;
        }
        if (min !== null && !allowedMinutes.includes(min)) {
          return false;
        }
        if (
          min !== null &&
          HH - hr <= 1 &&
          min < mm &&
          this.departureDate === this.returnDate
        ) {
          return false;
        }
        return true;
      }
    },

    checkDepartureDateValidity() {
      if (
        this.returnDate &&
        this.departureDate &&
        typeof this.departureDate === 'string' &&
        typeof this.returnDate === 'string'
      ) {
        const [dayD, monthD, yearD] = this.departureDate
          .split('/')
          .map((part: string) => parseInt(part, 10));
        const [dayR, monthR, yearR] = this.returnDate
          .split('/')
          .map((part: string) => parseInt(part, 10));
        if (
          yearR - yearD < 0 ||
          (yearR - yearD <= 0 && monthR - monthD < 0) ||
          (yearR - yearD <= 0 && monthR - monthD <= 0 && dayR - dayD < 0)
        ) {
          this.returnDate = null;
        }
      }
    },
    checkDepartureTimeValidity() {
      if (this.returnTime && this.departureTime) {
        if (this.departureDate === this.returnDate) {
          const hourOfDeparture = parseInt(
            this.departureTime.split(':')[0],
            10
          );
          const hourOfReturn = parseInt(this.returnTime.split(':')[0], 10);
          if (hourOfReturn - hourOfDeparture <= 0) {
            this.returnTime = null;
          }
        }
      }
    },
    async searchAgency() {
      this.activeSearch = true;
      this.city = null;
      this.cities = null;
      this.cityCode = null;
      if (this.searchInput?.length === 0) {
        this.activeSearch = false;
      }
      if (this.searchTimeout) {
        clearTimeout(this.searchTimeout);
      }
      if (this.abortController) {
        this.abortController.abort();
      }
      this.searchTimeout = setTimeout(this.performSearch, 1000);
    },

    async performSearch() {
      this.abortController = new AbortController();
      if (this.searchInput) {
        const formattedInput = this.searchInput.trim();
        const postalCode2chars = /^\d{2}$/;
        const postalCode5chars = /^\d{5}$/;
        const cityName =
          /^[a-zA-Z]{1,}\s?\D?[a-zA-Z]{1,}?\s?\D?[a-zA-Z]{1,}?\s?\D?[a-zA-Z]{1,}?$/;
        const cityPlusPostalCode = /^(.+?)[\s,-]*(\d{2,5})$/;
        const matchCityAndCode = formattedInput.match(cityPlusPostalCode);
        let response: Response | null = null;
        this.cityList = true;
        if (formattedInput.match(postalCode5chars)) {
          response = await fetch(
            `https://geo.api.gouv.fr/communes?codePostal=${formattedInput}`,
            { signal: this.abortController.signal }
          );
          this.cityCode = formattedInput;
        } else if (formattedInput.match(postalCode2chars)) {
          response = await fetch(
            `https://geo.api.gouv.fr/communes?codeDepartement=${formattedInput}&boost=population&limit=10`,
            { signal: this.abortController.signal }
          );
        } else if (formattedInput.match(cityName)) {
          response = await fetch(
            `https://geo.api.gouv.fr/communes?nom=${formattedInput}&boost=population&limit=10`,
            { signal: this.abortController.signal }
          );
        } else if (matchCityAndCode) {
          const name = matchCityAndCode[1].trim();
          const code = matchCityAndCode[2].trim().slice(0, 2);
          response = await fetch(
            `https://geo.api.gouv.fr/communes?nom=${name}&codeDepartement=${code}`,
            { signal: this.abortController.signal }
          );
        }
        if (response !== null) {
          this.cities = await response.json();
          this.activeSearch = false;
        } else {
          this.cityList = false;
          this.activeSearch = false;
        }
      }
    },
    formatUserInput(city: city) {
      this.city = city;

      this.searchInput = this.city?.nom;
      if (this.city.codesPostaux.length === 1) {
        this.cityCode = this.city.codesPostaux[0];
      } else {
        this.cityCode = null;
      }
    },

    parseDate(date: string) {
      const [day, month, year] = date.split('/').map(Number);
      return new Date(year, month - 1, day);
    },

    parseTime(time: string) {
      const [HH, mm] = time.split(':').map(Number);
      return [HH, mm];
    },
    calculateDaysBetween() {
      if (
        this.departureDate &&
        this.returnDate &&
        this.departureTime &&
        this.returnTime &&
        this.departureDate !== this.returnDate
      ) {
        const departureDay = this.parseDate(this.departureDate);
        const returnDay = this.parseDate(this.returnDate);
        const timeDiff = Math.abs(returnDay.getTime() - departureDay.getTime());
        const departureTime = this.parseTime(this.departureTime);
        const returnTime = this.parseTime(this.returnTime);
        const daysBetween = timeDiff / (1000 * 60 * 60 * 24);
        if (
          returnTime[0] > departureTime[0] ||
          (returnTime[0] === departureTime[0] &&
            returnTime[1] > departureTime[1])
        ) {
          this.rentalDays = daysBetween + 1;
        } else {
          this.rentalDays = daysBetween;
        }
      } else {
        this.rentalDays = 1;
      }
    },
  },
});
