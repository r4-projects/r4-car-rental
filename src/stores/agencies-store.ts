import { defineStore } from 'pinia';
import commonStreeNames from '../assets/commonStreeName.json';
import { useTravelSettingsStore } from './travel-settings-store';
const TSS = useTravelSettingsStore();

interface FakeAgency {
  name: string | null;
  streetNumber: string | null;
  streetName: string | null;
  postalCode: string | null;
  cityName: string | null;
}
interface State {
  openAgencyMenu: boolean;
  selectedAgency: object | null;
  fakeAgency: FakeAgency | null;
}

export const useAgenciesStore = defineStore('agencies', {
  state: (): State => ({
    openAgencyMenu: false,
    selectedAgency: null,
    fakeAgency: null,
  }),
  actions: {
    openAgencySelectionDialog() {
      this.openAgencyMenu = true;
    },
    simulateAgency() {
      const randomStreetName = Math.floor(Math.random() * 29);
      const streetNumber = (Math.floor(Math.random() * 50) + 1).toString();
      if (TSS.city && this.fakeAgency?.postalCode !== TSS.cityCode) {
        this.fakeAgency = {
          name: `Agence de ${TSS.city.nom}`,
          streetNumber: streetNumber,
          streetName: commonStreeNames[randomStreetName],
          postalCode: TSS.cityCode,
          cityName: TSS.city.nom,
        };
      }
    },
  },
});
