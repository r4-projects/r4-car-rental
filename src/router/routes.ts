import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: '/login', component: () => import('pages/LogInPage.vue') },
      { path: '/signup', component: () => import('pages/SignUpPage.vue') },
      {
        path: '/select-car',
        component: () => import('pages/SelectCarPage.vue'),
      },
      {
        path: '/select-options',
        component: () => import('pages/SelectOptionsPage.vue'),
      },
    ],
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
