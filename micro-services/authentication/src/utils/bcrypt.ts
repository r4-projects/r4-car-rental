import bcrypt from 'bcrypt'
const saltRounds = 10;

async function getHashedPassword (password: string): Promise<string> {
  const hash = await bcrypt.hash(password, saltRounds);
  return hash;
}

async function comparePasswords (password: string, DBHash: string): Promise<boolean> {
  return bcrypt.compare(password, DBHash)
}

export { getHashedPassword, comparePasswords}
