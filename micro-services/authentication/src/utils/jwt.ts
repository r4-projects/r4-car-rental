import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()

const jwt_secret : string = process.env.JWT_PASSWORD as string

if (!jwt_secret) {
  throw new Error('JWT secret is not defined. Please set the JWT_PASSWORD environment variable.');
}

function signToken (email: string){
const payload = {
  email : email
};
const options = {
  expiresIn : '1h'
};

const token = jwt.sign(payload, jwt_secret, options);
return token;
}

export {signToken}
