import { Router } from 'express';
import userRoutes from './user-routes';

const router = Router();

// Ajoutez les routes ici
router.use('/user', userRoutes);

export default router;
