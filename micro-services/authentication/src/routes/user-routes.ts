import  { createUser, connectUser } from '../controllers/user'
import { Router } from 'express';
const router = Router();

router.post('/login', connectUser);
router.post('/create-user', createUser);

export default router;
