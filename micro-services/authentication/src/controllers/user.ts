import { UserModel } from '../schemas';
import { Request, Response } from 'express'
import { signToken } from '../utils/jwt';
import { connectToDatabase, disconnectFromDatabase } from '../config/database';
import { getHashedPassword, comparePasswords } from '../utils/bcrypt';

async function createUser( req: Request, res: Response) {
  try {
    await connectToDatabase()
    const userExists = await UserModel.findOne({ email: req.body.email });
    if (userExists) {
      res.status(400).send({message : 'User already exists'})
    } else {
      const email : string = req.body.email
      const encryptedPassword = await getHashedPassword(req.body.email)

      const user = new UserModel({email: email, password : encryptedPassword});
      await user.save();
      await disconnectFromDatabase()
      const token = signToken(req.body.email)
      res.status(200).send({token})
    }
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(`Error creating user: ${error.message}`);
    }
  }
}

async function connectUser ( req: Request, res: Response) {
  const email : string = req.body.email
  await connectToDatabase()
  const user = await UserModel.findOne({email})
  if (user) {
    try {
      const passwordMatch = await comparePasswords(email, user.password)
      if (passwordMatch) {
        const token = signToken(email)
        res.status(200).send({token})
      } else {
        res.status(400).send({message : 'Wrong credentials'})
      }
    } catch (e) {
      if (e instanceof Error) {
        console.log('error')
        throw new Error (`Password don't match ${e.message}`)
      }
    }
  } else {
    throw new Error('User does not exists')
  }
  await disconnectFromDatabase()
}

export { createUser, connectUser };
