
import express, {Application} from 'express';
import router from './routes/index';
import cors from 'cors'

const app : Application = express();

app.use(express.json());

const corsOptions = {
  origin: 'http://localhost:9000', // Replace with your client's origin
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  allowedHeaders: ['Content-Type', 'Authorization']
};

app.use(cors(corsOptions))

app.use('/api', router);

export default app
