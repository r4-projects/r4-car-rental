import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

async function connectToDatabase() {
  const username = process.env.DATABASE_USERNAME;
  const password = process.env.DATABASE_PASSWORD;
  await mongoose.connect(
    `mongodb+srv://${username}:${password}@cluster0.mufvny7.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0`
  )
  .then(() => console.log('MongoDB connected'))
  .catch(err => console.log('Error: ', err));
}

async function disconnectFromDatabase() {
  await mongoose.disconnect();
  console.log('Mongo disconnected')
}

export { connectToDatabase, disconnectFromDatabase };
